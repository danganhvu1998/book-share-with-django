# chat/routing.py
from django.urls import path

from . import consumers

websocket_urlpatterns = [
    path('ws/testChannels/<str:room_name>/', consumers.ChatConsumer),
]

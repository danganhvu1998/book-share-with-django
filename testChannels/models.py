from django.db import models
from django.shortcuts import render, redirect
from django.http import Http404
from django.utils import timezone


class Message(models.Model):
    content = models.CharField(max_length=200)
    user_id = models.CharField(max_length=100, default=0)
    posted_at = models.DateTimeField('date published')
    

'''class Comment(models.Model):
    text = models.TextField()
    posted_at = models.DateTimeField('date published')
    book_id = models.ForeignKey(Book, related_name="comments", on_delete=models.CASCADE, default=0)
    user_id = models.ForeignKey(User, related_name="comments", on_delete=models.CASCADE, default=0)
'''
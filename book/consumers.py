from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from django.utils import timezone
from django.contrib.auth.models import User
from .models import Book, Comment
import json

class CommentConsumer(WebsocketConsumer):
    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        print(self.room_name)
        self.room_group_name = 'chat_%s' % self.room_name
        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )
        self.accept()
    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )
    # Receive message from WebSocket
    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        text = text_data_json['text']
        user_id= text_data_json['user_id']
        book_id= text_data_json['book_id']
        posted_at= timezone.now()
        
        try:
            book = Book.objects.get(pk=book_id)
        except Book.DoesNotExist:
            raise Http404("Book does not exist")

        try:
            user = User.objects.get(pk=user_id)
        except User.DoesNotExist:
            raise Http404("User does not exist")
        
        newComment= Comment (
            text= text, 
            user_id= user,
            book_id= book,
            posted_at= posted_at
        )

        newComment.save()
        print( str(text_data_json['user_id']) )
        # Send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {   'type': 'chat_message',
                'comment':  {   'text': text,
                                'user_id': text_data_json['user_id'],
                                'username': text_data_json['username'],
                                'book_id': text_data_json['book_id'],
                                'posted_at':  str(posted_at),
                            }
            }
        )
    # Receive message from room group
    def chat_message(self, event):
        comment = event['comment']
        # Send message to WebSocket
        self.send(text_data=json.dumps({'comment': comment}))

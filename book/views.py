from django.shortcuts import render, redirect
from django.http import Http404, JsonResponse
from django.utils import timezone
from django import forms
from .models import Book,Comment
from django.contrib.auth.decorators import login_required
from django.utils.safestring import mark_safe
import json


# Create your views here.
def index(request):
    books = Book.objects.order_by('posted_at')
    print(books)
    context ={
        'books':books
    }

    return render(request, 'book/index.html', context)
@login_required

def create(request):
    if request.method == 'POST':
        try:
            Book.objects.get(title=request.POST['title'])
            context = {
                'announce' : 'Existed Book!'
            }
            return render(request,'book/create.html',context)
        except Book.DoesNotExist:
            Book(
                title=request.POST['title'],
                intro=request.POST['intro'],
                image_url=request.POST['image_url'],
                posted_at=timezone.now(),
                user_id=request.user,
                status=1).save()
            return redirect(index)
    return render(request,'book/create.html')
def edit(request, book_id):
    try:
        book = Book.objects.get(pk=book_id)
    except Book.DoesNotExist:
        raise Http404("Book does not exist")

    if request.user != book.user_id:
        raise Http404("You don't have persimission")
    
    if request.method == 'POST':
        book.title = request.POST['title']
        book.intro = request.POST['intro']
        book.image_url = request.POST['image_url']
        book.save()
        return redirect('detail', book.id)
    context = {'book' : book}
    return render(request, 'book/edit.html', context)

def detail(request, book_id):
    try:
        book = Book.objects.get(pk=book_id)
    except Book.DoesNotExist:
        raise Http404("Book does not exist")
    comments= Comment.objects.all()
    context = {
        'book' : book,
        'room_name_json': mark_safe(json.dumps(book_id)),
        'comments': comments,
    }
    return render(request,'book/detail.html',context)


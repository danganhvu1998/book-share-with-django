# book/routing.py
from django.urls import path

from . import consumers

websocket_urlpatterns = [
    path('ws/book/detail/<str:room_name>/', consumers.CommentConsumer),
]

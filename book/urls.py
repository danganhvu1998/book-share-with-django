from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path(r'create/', views.create, name='addBook'),
    path(r'edit/<int:book_id>/', views.edit, name='edit'),
    path(r'detail/<int:book_id>/', views.detail, name='detail'),

]

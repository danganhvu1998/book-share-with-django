from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.http import Http404
from django.utils import timezone

from django.http import JsonResponse as jsonResponse
from django.contrib.auth.models import User
import django.contrib.auth.hashers as hashers

# Create your views here.
def home(request):
  return render(request, 'fixedPage/home_temp.html', {})

def checkCreateRequest(request):
    #print("check", len(User.objects.filter(username=request["username"])))
    if(len(User.objects.filter(username=request["username"]))):
        return 0
    if(len(User.objects.filter(email=request["email"]))):
        return 0
    return 1

def create(request):
    if request.method == 'POST':
        #return HttpResponse(request)
        if(checkCreateRequest(request.POST)!=1):
            return jsonResponse(request.POST)
        newUser = User(
            password = hashers.make_password(request.POST['password']),
            username = request.POST['username'],
            first_name = request.POST['first_name'],
            last_name = request.POST['last_name'],
            is_active = 1,
            email = request.POST['email'],
            date_joined = timezone.now()
        )
        newUser.save()
        return redirect("login")
    else:
        return render(request, 'registration/register.html')